# kumori client-go

Go client for Kumori objects (informers, listers...) in Kubernetes

## Ejemplo de uso

https://gitlab.com/kumori/kv2/httpinbound-controller/blob/ticket1488/go.mod

https://gitlab.com/kumori/kv2/httpinbound-controller/blob/ticket1488/cmd/controller-manager/main.go

https://gitlab.com/kumori/kv2/httpinbound-controller/blob/ticket1488/pkg/controllers/kukucert/controller.go

## Sobre la ubicación de este proyecto

Típicamente, este proyecto *debería* residir en kumori/kv2.

Sin embargo, Gitlab.com no procesa correctamente las peticiones http generadas
por el comando go-get en el caso de subproyectos.


https://gitlab.com/gitlab-org/gitlab-ce/issues/37832#note_201038683

I used ...

-  GitLab Enterprise Edition 12.1.0-pre
-  GO v1.13beta1 (using Docker image golang:1.13-rc-buster)

... and I'm still able to reproduce the problem of go-get + Gitlab-subgroups:

We can see the problem executing the http request related to the go-get command.

```
// Works fine without subgroups

root@f7fa92dbdec3:/go#  curl https://gitlab.com/mygroup/mypackage?go-get=1
<html><head><meta name="go-import" content="gitlab.com/mygroup/mypackage git https://gitlab.com/mygroup/mypackage.git" /></head></html>

// Fails with subgroups

root@f7fa92dbdec3:/go#  curl https://gitlab.com/mygroup/mysubgroup/mypackage?go-get=1
<html><head><meta name="go-import" content="gitlab.com/mygroup/mysubgroup git https://gitlab.com/mygroup/mysubgroup.git" /></head></html>
```

## Ambassador

Este paquete incluye los objetos (Informers, Listers...) para Ambassador, en
tanto que también son utilizados por controladores de Kumori.

Nota: muy discutible que este paquete sea adecuado para objetos de Ambassador.

## Code-Generator

Durante el ciclo de vida de este proyecto es necesario generar código utilizando
la herramienta code-generator de Kubernetes.</br>
Por tanto, es necesario tener instalado en el PC esta herramienta, consistente
en varios ejecutables.

*IMPORTANTE* la versión del code-generator debe ser acorde a las dependencias:<br/>
- cluster Kubernetes: 1.16.10
- code-generator: kubernetes-1.16.10
- client-go:
- apimachinery:
- api:

### Obtener y compilar el code-generator

Simplemente un go-get de la versión deseada:
```
$ go get -v k8s.io/code-generator@kubernetes-1.16.10
get "k8s.io/code-generator": found meta tag get.metaImport{Prefix:"k8s.io/code-generator", VCS:"git", RepoRoot:"https://github.com/kubernetes/code-generator"} at //k8s.io/code-generator?go-get=1
go: finding k8s.io/code-generator kubernetes-1.16.10
[...]
// OJO: ha "decidido" que se baja este release (gestión suya del pkg)
go: downloading k8s.io/code-generator v0.16.13-rc.0
[...]
// Este error final no lo entiendo
build k8s.io/code-generator: cannot load k8s.io/code-generator: no Go source files
```

El paquete se ha instalado en mi GOPATH:
```
$ ls $GOPATH/pkg/mod/k8s.io | grep code-generator
code-generator@v0.16.13-rc.0
```

Y ahora lo compilo:
```
$ go install $GOPATH/pkg/mod/k8s.io/code-generator@v0.16.13-rc.0/cmd/{defaulter-gen,client-gen,lister-gen,informer-gen,deepcopy-gen}
```

Los CINCO ejecutables se dejan aquí (OJO: machacando si había alguna compilación previa!!).
(deben tener fecha actual)
```
$ ls -l $GOPATH/bin | grep gen
-rwxr-xr-x 1 jvalero jvalero  9386282 jul  2 16:54 client-gen
-rwxr-xr-x 1 jvalero jvalero  9113774 jul  2 16:54 deepcopy-gen
-rwxr-xr-x 1 jvalero jvalero  9075707 jul  2 16:54 defaulter-gen
-rwxr-xr-x 1 jvalero jvalero  9217920 jul  2 16:54 informer-gen
-rwxr-xr-x 1 jvalero jvalero  9076244 jul  2 16:54 lister-gen
```

En el paquete del code-generator hay unos scritps que vamos a usar: les damos permisos de ejecución:
```
chmod +x $GOPATH/pkg/mod/k8s.io/code-generator@v0.16.13-rc.0/generate-groups.sh
chmod +x $GOPATH/pkg/mod/k8s.io/code-generator@v0.16.13-rc.0/generate-internal-groups.sh
```

Ya tenemos preparado el code-generator.

### Generación de código

Una vez hemos preparado el directorio con el "material" para el code-generator,
podemos regenerar el código utilizando el script client-go/hack/update-codegen.sh.

Este script debe hacer referencia a la ubicación correcta del paquete code-generator:

```
update-codegen.sh:

#!/usr/bin/env bash
[...]
# <<< BE CAREFUL, SELECT THE RIGHT LOCATION
CODEGEN_PKG=$GOPATH/pkg/mod/k8s.io/code-generator@v0.16.13-rc.0/
[...]
```

## Cómo añadir un nuevo CRD

Supongamos que queremos añadir un nuevo CRD llamado kuku-brether. Debemos:

- Añadir el fichero kuku-brether-crd.yaml, con su definición, en el directorio
  /artifacts. Esto no es necesario para la librería! Pero parece un lugar adecuado
  en el que guardar los CRDs.
- Añadir el fichero kuku-brether.go en el directorio /pkg/apis/kumori/v1, con
  los structs que representan al CRD. Típicamente serán los tipos KukuBrether y
	KukuBretherList{},
- Modificar la función addKnownTypes del fichero /pkg/apis/kumori/v1/register.go
  con los nuevos tipos.
- Regenerar el código
- Añadir un nuevo tag en https://gitlab.com/mandanga-public/sub1/sub2/client-go/-/tags

## go get y acceso a repositorio privados

Desde mi PC, al hacer un "go get" de este paquete, me pregunta el usuario y
contraseña de acceso (está usando https para el clonado).

```
$ go get  -v gitlab.com/mandanga-public/sub1/sub2/client-go
Fetching https://gitlab.com/mandanga-public/sub1/sub2/client-go?go-get=1
Parsing meta tags from https://gitlab.com/mandanga-public/sub1/sub2/client-go?go-get=1 (status code 200)
get "gitlab.com/mandanga-public/sub1/sub2/client-go": found meta tag get.metaImport{Prefix:"gitlab.com/mandanga-public/sub1/sub2/client-go", VCS:"git", RepoRoot:"https://gitlab.com/mandanga-public/sub1/sub2/client-go.git"} at https://gitlab.com/mandanga-public/sub1/sub2/client-go?go-get=1
Username for 'https://gitlab.com': juavabar
Password for 'https://juavabar@gitlab.com':
go: extracting gitlab.com/mandanga-public/sub1/sub2/client-go v0.0.2
Fetching https://gitlab.com/kumori?go-get=1
Parsing meta tags from https://gitlab.com/kumori?go-get=1 (status code 200)
Fetching https://gitlab.com?go-get=1
Parsing meta tags from https://gitlab.com?go-get=1 (status code 200)
```

Pero en el PC de Albert, le da un error "unknown revision".

He probado a ejecutarlo en un contenedor nuevo cuya imagen incluye GO, y me da
un error distinto pero más explícito: (el propio error nos propone soluciones)

```
root@0bcc83cd2259:/kk# go get gitlab.com/mandanga-public/sub1/sub2/client-go
go get gitlab.com/mandanga-public/sub1/sub2/client-go: git ls-remote -q origin in /go/pkg/mod/cache/vcs/1f5ccad89a7ede235b28acc1b6e5cdf7ce8aba2e6d1a03c912788b77312a0ff6: exit status 128:
  fatal: could not read Username for 'https://gitlab.com': terminal prompts disabled
Confirm the import path was entered correctly.
If this is a private repository, see https://golang.org/doc/faq#git_https for additional information.
```

Estableciendo la variable GIT_TERMINAL_PROMPT, ya me lo puedo descargar.
```
root@0bcc83cd2259:~# env GIT_TERMINAL_PROMPT=1 go get -v gitlab.com/mandanga-public/sub1/sub2/client-go
get "gitlab.com/mandanga-public/sub1/sub2/client-go": found meta tag get.metaImport{Prefix:"gitlab.com/mandanga-public/sub1/sub2/client-go", VCS:"git", RepoRoot:"https://gitlab.com/mandanga-public/sub1/sub2/client-go.git"} at //gitlab.com/mandanga-public/sub1/sub2/client-go?go-get=1
gitlab.com/mandanga-public/sub1/sub2/client-go (download)
Username for 'https://gitlab.com': juavabar
Password for 'https://juavabar@gitlab.com':
package gitlab.com/mandanga-public/sub1/sub2/client-go: no Go files in /go/src/gitlab.com/mandanga-public/sub1/sub2/client-go
```

No he probado a aplicar lo que se recomienda en https://golang.org/doc/faq#git_https.

Pero Albert sí que lo ha utilizado (en concreto lo de "Git can also be configured to use SSH in place of HTTPS"), y le ha funcionado OK.

Otras referencias a este problema:

https://stackoverflow.com/questions/32232655/go-get-results-in-terminal-prompts-disabled-error-for-github-private-repo
