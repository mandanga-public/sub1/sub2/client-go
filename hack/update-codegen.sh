#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

BASE_PATH=$(dirname "${BASH_SOURCE[0]}")/..

# ------------------------------------------------------------------------------
#
# BE CAREFUL, SELECT THE RIGHT LOCATION OF CODE-GENERATOR PACKAGE
#
# ------------------------------------------------------------------------------
CODEGEN_PKG=$GOPATH/pkg/mod/k8s.io/code-generator@v0.16.13-rc.0/


# generate the code with:
# --output-base    because this script should also be able to run inside the vendor dir of
#                  k8s.io/kubernetes. The output-base is needed for the generators to output into the vendor dir
#                  instead of the $GOPATH directly. For normal projects this can be dropped.

# JValero:
# The generator should run with something like this ...
#
#   "${CODEGEN_PKG}"/generate-groups.sh \
#     all \
#     pkg/generated \
#     client-go/pkg/apis \
#     kumori:v1 \
#     --output-base "${BASE_PATH}/.." \
#     --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt
#
# ... but I doesnt works for me, so I had splitted it in two parts and move
# directories !!

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated \
  gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis \
  kumori:v1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# $BASE_PATH/gitlab.com --> $BASE_PATH/pkg/generated
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated
mkdir $BASE_PATH/pkg/generated
mv $BASE_PATH/gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated/* $BASE_PATH/pkg/generated
rm -rf $BASE_PATH/gitlab.com

# Generating just the copy function
"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis" \
  kumori:v1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt


# Repeat the work, but for ambassador objects (included in this package)

"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated_ambassador \
  gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis_ambassador \
  getambassador.io:v1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

rm -rf $BASE_PATH/pkg/generated_ambassador
mkdir $BASE_PATH/pkg/generated_ambassador
mv $BASE_PATH/gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated_ambassador/* $BASE_PATH/pkg/generated_ambassador
rm -rf $BASE_PATH/gitlab.com

"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis_ambassador" \
  getambassador.io:v1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt
