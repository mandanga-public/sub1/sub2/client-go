module gitlab.com/mandanga-public/sub1/sub2/client-go


go 1.12

// ORIGINAL REQUIRE: (using clusters kubernetes v1.16.10)
// require (
//   github.com/sirupsen/logrus v1.6.0
//   k8s.io/client-go    kubernetes-1.16.10
//   k8s.io/api          kubernetes-1.16.10
//   k8s.io/apimachinery kubernetes-1.16.10
// )

require (
	github.com/sirupsen/logrus v1.6.0
	k8s.io/apimachinery v0.16.11-rc.0
	k8s.io/client-go v0.16.10
	k8s.io/code-generator v0.16.13-rc.0 // indirect
)
