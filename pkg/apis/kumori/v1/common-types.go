package v1

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"
	resource "k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// Internal types and constants

////////////////////
// INTERNAL TYPES //
////////////////////

// KukuVolumeSpec describes the specification of a given volume
type KukuVolumeSpec struct {
	FileSystem *FileSystemType   `json:"fileSystem"`
	MountPoint *string           `json:"mountpoint,omitempty"`
	Size       resource.Quantity `json:"size"`
}

// KumoriChannels contains information about the channels of a given KukuService or KukuComponent
type KumoriChannels struct {
	Provides *[]KumoriChannel `json:"provides,omitempty"`
	Requires *[]KumoriChannel `json:"requires,omitempty"`
}

// KumoriChannel defines a service or component channel
type KumoriChannel struct {
	Name     string            `json:"name"`
	Type     KumoriChannelType `json:"type"`
	Protocol string            `json:"protocol"`
	Port     *int32            `json:"port,omitempty"`
	Children *[]string         `json:"children,omitempty"`
}

// KumoriConfigurationDescription contains information about the declared configuration parameters
// and resources in KukuComponents and KukuServices
type KumoriConfigurationDescription struct {
	Parameters *[]KumoriParameterDescription `json:"parameters,omitempty"`
	Resources  *[]KumoriResourceDescription  `json:"resources,omitempty"`
}

// KumoriParameterDescription contains the declaration of a configuration parameter
// TODO: use an enumeration for "Type"
// TODO: optional default value
type KumoriParameterDescription struct {
	Name    string `json:"name"`
	Type    string `json:"type"`
	Default []byte `protobuf:"bytes,1,opt,name=default"`
}

// KumoriResourceDescription contains the declaration of a configuration resource
// TODO: use an enumeration for "Type"
type KumoriResourceDescription struct {
	Name string       `json:"name"`
	Type ResourceType `json:"type"`
}

// KumoriContainer contains information about a Docker image used in a component.
type KumoriContainer struct {
	Name       string            `json:"name"`
	Image      string            `json:"image"`
	Cmd        *[]string         `json:"cmd,omitempty"`
	User       *Owner            `json:"user,omitempty"`
	Args       *[]string         `json:"args,omitempty"`
	FileSystem *[]FileSystemItem `json:"filesystem,omitempty"`
	Env        *[]EnvItem        `json:"env,omitempty"`
	Secret     *string           `json:"secret,omitempty"`
}

// Owner contains information about the owner of a file, proces, ...
type Owner struct {
	UserID  *int64 `json:"userid"`
	GroupID *int64 `json:"groupid"`
}

// Permissions contains information about a filesystem element permissions.
type Permissions struct {
	User *Owner `json:"owner,omitempty"`
	Mode *int32 `json:"mode,omitempty"`
}

// FileSystemItem contains information to expose data on a container filesystem
type FileSystemItem struct {
	Data   *runtime.RawExtension `json:"data,omitempty"`
	Format *string               `json:"format,omitempty"`
	Path   string                `json:"path"`
	Perms  *Permissions          `json:"perms,omitempty"`
	Size   *int32                `json:"size,omitempty"`
	Unit   *string               `json:"unit,omitempty"`
}

// Content is used to retrieve the content for a given element (files, environment
// variables). The data can be referenced from an element in context using `Ref`(for
// example, a configuration parameter) or directly set using `Data`.
// type Content struct {
// 	Data *string `json:"data,omitempty"`
// 	Ref  *string `json:"ref,omitempty"`
// }

// VolumeRef is used to reference a volume when mounted in a given container
type VolumeRef struct {
	Ref *string `json:"ref,omitempty"`
}

// EnvItem contains information to expose data in a container as an environment variable
type EnvItem struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// KumoriRole contains information about a Role in a KukuService
type KumoriRole struct {
	Name              string               `json:"name"`
	ComponentSelector metav1.LabelSelector `json:"componentSelector"`
}

// KumoriConnector contains information about a connector in a KukuService
type KumoriConnector struct {
	Type     string            `json:"type"`
	Name     string            `json:"name"`
	Port     *int32            `json:"port,omitempty"`
	Depended *[]KumoriEndpoint `json:"depended,omitempty"`
	Provided *[]KumoriEndpoint `json:"provided,omitempty"`
}

// KumoriEndpoint defines an endpoint in a KukuConnector. If the Role is nil then represents a
// serviche channel. Otherwirse, represents a role channel.
type KumoriEndpoint struct {
	Name string  `json:"name"`
	Role *string `json:"role,omitempty"`
}

// KumoriConfiguration contains the values assigned to the KukuService declared parameters and
// the assigned resources.
type KumoriConfiguration struct {
	// Parameters map[string]unustructuredv1.Unstructured `json:"parameters"`
	// Resources  map[string]unustructuredv1.Unstructured `json:"resources"`
	// Parameters []byte `protobuf:"bytes,1,opt,name=parameters"`
	// Resources  []byte `protobuf:"bytes,1,opt,name=resources"`
	Parameters *map[string]Unstructured `json:"parameters,omitempty"`
	Resources  *map[string]Unstructured `json:"resources,omitempty"`
}

// Unstructured represents a JSON object with unknown structure.
type Unstructured map[string]interface{}

// DeepCopy copies an Unstructured.
// TODO: shitty conversion using json.Marshal and json.Unmarshal
func (p *Unstructured) DeepCopy() *Unstructured {
	data, err := json.Marshal(*p)
	if err != nil {
		log.Error("Unstructured.DeepCopy. Error: ", err)
	}
	element := new(Unstructured)
	json.Unmarshal(data, &element)
	return element
	// return p
}

// type KumoriConfigurationResources map[string]interface{}

// KumoriRoleArrangement contains information about each role instances resource requirements.
type KumoriRoleArrangement struct {
	Name          string `json:"name"`
	Instances     int32  `json:"instances"`
	Maxinstances  *int32 `json:"maxinstances,omitempty"`
	Mininstances  *int32 `json:"mininstances,omitempty"`
	CPU           string `json:"cpu"`
	Memory        string `json:"memory"`
	Ioperf        string `json:"ioperf"`
	Iopsintensive bool   `json:"iopsintensive"`
	Bandwidth     string `json:"bandwidth"`
	Resilience    int32  `json:"resilience"`
}

// KumoriRoleSLA contains the SLA of each role
type KumoriRoleSLA struct {
	Role       string        `json:"role"`
	Type       string        `json:"type"`
	Parameters *Unstructured `json:"parameters"`
}

///////////////
// CONSTANTS //
///////////////

// FileSystemType defines the file system types supported in PersistenVolumes
type FileSystemType string

const (
	// XFS represents the xfs file system
	XFS FileSystemType = "xfs"
	// EXT4 represents the ext4 file system
	EXT4 FileSystemType = "ext4"
)

// ResourceType is used as an enumerate to fix which resource types are going to be
// admitted by this controller.
type ResourceType string

const (
	// PersistentVolumeType is the type of a volume storing data to be persisted
	PersistentVolumeType ResourceType = "persistentvolume"
	// VolatileVolumeType is the type of a volume storing volatile data
	VolatileVolumeType ResourceType = "volatilevolume"
	// ClientCertType is the type of a certificate used by clients to identify themselves
	ClientCertType ResourceType = "clientcert"
	// ServerCertType is the type of a certificate used by servers to identify a given server in a secure connection
	ServerCertType ResourceType = "servercert"
	// VHostType is the type of stores a virtual host domain
	VHostType ResourceType = "vhost"
)

// KumoriChannelType defines the Kumori channels type
type KumoriChannelType string

const (
	// KumoriSendChannel is a channel to publish messages
	KumoriSendChannel KumoriChannelType = "send"
	// KumoriReceiveChannel is a channel to subscribe to messages
	KumoriReceiveChannel KumoriChannelType = "receive"
	// KumoriRequestChannel is a channel to perform requests to other components
	KumoriRequestChannel KumoriChannelType = "request"
	// KumoriReplyChannel is a channel to attend requests
	KumoriReplyChannel KumoriChannelType = "reply"
	// KumoriDuplexChannel is a channel used for full duplex communications
	KumoriDuplexChannel KumoriChannelType = "duplex"
	// KumoriPushChannel is a channel used to send messages to a specific destination
	KumoriPushChannel KumoriChannelType = "push"
	// KumoriPullChannel is a channel used to receive messages
	KumoriPullChannel KumoriChannelType = "pull"
	// KumoriComboChannel combines several channels under a single name. Is used, for
	// example, to combine a push and a pull channels under a single combines channel for
	// full duplex communications.
	KumoriComboChannel KumoriChannelType = "combo"
)
