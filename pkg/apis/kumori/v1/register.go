// The register.go file contains the functions registering the new types we just
// created to the schema so the API server can recognise them.

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// SchemeGroupVersion is group version used to register these objects.
// Define your schema name and the version.
var SchemeGroupVersion = schema.GroupVersion{
	Group:   "kumori.systems",
	Version: "v1",
}

// Kind takes an unqualified kind and returns back a Group qualified GroupKind
// TBD - What is this?
func Kind(kind string) schema.GroupKind {
	return SchemeGroupVersion.WithKind(kind).GroupKind()
}

// Resource takes an unqualified resource and returns a Group qualified GroupResource
// TBD - What is this?
func Resource(resource string) schema.GroupResource {
	return SchemeGroupVersion.WithResource(resource).GroupResource()
}

var (
	// SchemeBuilder initializes a scheme builder.
	SchemeBuilder      runtime.SchemeBuilder
	localSchemeBuilder = &SchemeBuilder
	// AddToScheme is a global function that registers this API group & version to a scheme
	AddToScheme = SchemeBuilder.AddToScheme
)

func init() {
	// We only register manually written functions here. The registration of the
	// generated functions takes place in the generated files. The separation
	// makes the code compile even when the generated files are missing.
	localSchemeBuilder.Register(addKnownTypes)
}

// Adds the list of known types to Scheme.
func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion,
		&KukuVHost{},
		&KukuVHostList{},
		&KukuCert{},
		&KukuCertList{},
		&KukuHttpInbound{},
		&KukuHttpInboundList{},
		&KukuLink{},
		&KukuLinkList{},
		&KukuCN{},
		&KukuCNList{},
		&KukuComponent{},
		&KukuComponentList{},
		&KukuService{},
		&KukuServiceList{},
		&KukuDeployment{},
		&KukuDeploymentList{},
		&KukuPersistentVolume{},
		&KukuPersistentVolumeList{},
		&V3Deployment{},
		&V3DeploymentList{},
	)
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}
