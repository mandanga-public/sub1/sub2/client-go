package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuPersistentVolume is a top-level type
type KukuPersistentVolume struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuPersistentVolumeSpec `json:"spec"`
	// Status						KukuComponentStatus `json:"status"`
}

// KukuPersistentVolumeSpec is the custom spec of KukuPersistentVolume
type KukuPersistentVolumeSpec struct {
	KukuVolumeSpec `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuPersistentVolumeList is a list of KukuPersistentVolume resources
type KukuPersistentVolumeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuPersistentVolume `json:"items"`
}

