package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuLink is a top-level type
type KukuLink struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuLinkSpec `json:"spec"`
}

// KukuLinkElement contains the deployment and channel of one side of the link
type KukuLinkElement struct {
	Deployment string `json:"deployment"`
	Channel    string `json:"channel"`
}

// KukuLinkSpec is the custom spec of KukuLink
type KukuLinkSpec struct {
	Element1 KukuLinkElement `json:"element1"`
	Element2 KukuLinkElement `json:"element2"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuLinkList is a list of KukuLink resources
type KukuLinkList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuLink `json:"items"`
}
