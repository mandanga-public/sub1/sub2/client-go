package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// V3Deployment is a top-level type
type V3Deployment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              DeploymentSpec `json:"spec"`
	// Status						DeploymentStatus `json:"status"`
}

// DeploymentSpec is the custom spec of Deployment
type DeploymentSpec struct {
	Element     `json:",inline"`
	Description DeploymentDescription `json:"description"`
}

// Element is blah
type Element struct {
	Ref   Reference `json:"ref"`
	Scope *Scope    `json:"scope,omitempty"`
	Spec  Version   `json:"spec"`
}

// Reference is blah
type Reference struct {
	Name    Name    `json:"name"`
	Kind    RefKind `json:"kind"`
	Domain  Domain  `json:"domain"`
	Version Version `json:"version"`
}

// DeploymentDescription is blah
type DeploymentDescription struct {
	Service Service `json:"service"`
	Qos     *Qos    `json:"QoS,omitempty"`
}

// Service is blah
type Service struct {
	Element     `json:",inline"`
	Description ServiceDescription `json:"description"`
}

// ServiceDescription is blah
type ServiceDescription struct {
	Config    Config                           `json:"config"`
	Srv       Srv                              `json:"srv"`
	Roles     map[string]Role                  `json:"role"`
	Connector map[string]ConnectorType         `json:"connector"`
	Links     map[string]*runtime.RawExtension `json:"link"`
}

// ConnectorType is blah
type ConnectorType string

const (
	// LBConnectorType is blah
	LBConnectorType ConnectorType = "lb"
	// FullConnectorType is blah
	FullConnectorType ConnectorType = "full"
)

// Role is blah
type Role struct {
	Component Component   `json:"component"`
	Size      RoleSize    `json:"rsize"`
	Updates   UpdatesType `json:"updates"`
}

// Component is blah
type Component struct {
	Element     `json:",inline"`
	Description ComponentDescription `json:"description"`
}

// ComponentDescription is blah
type ComponentDescription struct {
	Config  Config          `json:"config"`
	Srv     Srv             `json:"srv"`
	Size    ComponentSize   `json:"size"`
	Profile Profile         `json:"profile"`
	Codes   map[string]Code `json:"code"`
}

// ComponentSize is blah
type ComponentSize struct {
	CPU           string `json:"$_cpu"`
	Memory        string `json:"$_memory"`
	Ioperf        string `json:"$_ioperf"`
	Iopsintensive bool   `json:"$_iopsintensive"`
	Bandwidth     string `json:"$_bandwidth"`
}

// Profile is blah
type Profile struct {
	Threadability string `json:"threadability"`
}

// Code is blah
type Code struct {
	Name       string    `json:"name"`
	Image      Image     `json:"image"`
	Entrypoint *[]string `json:"entrypoint,omitempty"`
	Mapping    *Mapping  `json:"mapping,omitempty"`
}

// Mapping is blah
type Mapping struct {
	Env        *map[string]string `json:"env,omitempty"`
	FileSystem *[]MountPoint      `json:"filesystem,omitempty"`
}

// MountPoint contains information to expose data on a container filesystem
type MountPoint struct {
	Data   *runtime.RawExtension `json:"data,omitempty"`
	Format *string               `json:"format,omitempty"`
	Group  *int64                `json:"group,omitempty"`
	Mode   *int32                `json:"mode,omitempty"`
	Path   string                `json:"path"`
	Size   *int32                `json:"size,omitempty"`
	Unit   *string               `json:"unit,omitempty"`
	User   *int64                `json:"user,omitempty"`
}

// Image is blah
type Image struct {
	Hub *Hub   `json:"hub,omitempty"`
	Tag string `json:"tag"`
}

// Hub is blah
type Hub struct {
	Name   string  `json:"name"`
	Secret *string `json:"secret,omitempty"`
}

// RoleSize is blah
type RoleSize struct {
	Resilience   int32 `json:"$_resilience"`
	Instances    int32 `json:"$_instances"`
	MaxInstances int32 `json:"$_maxinstances"`
}

// UpdatesType is blah
type UpdatesType string

const (
	// FreeUpdatesType is blah
	FreeUpdatesType UpdatesType = "free"
)

// Config is blah
type Config struct {
	Parameters *map[string]runtime.RawExtension `json:"parameter,omitempty"`
	Resource   *map[string]runtime.RawExtension `json:"resource,omitempty"`
}

// Srv is blah
type Srv struct {
	Duplex  *map[string]Duplex `json:"duplex,omitempty"`
	Clients *map[string]Client `json:"client,omitempty"`
	Servers *map[string]Server `json:"server,ompitempty"`
}

// Duplex is blah
type Duplex Server

// Client is blah
type Client struct {
	Protocol SrvProtocol `json:"protocol"`
}

// Server is blah
type Server struct {
	Protocol SrvProtocol `json:"protocol"`
	Port     int32       `json:"port"`
}

// SrvProtocol is blah
type SrvProtocol string

const (
	// HTTPSrvProtocol is blah
	HTTPSrvProtocol SrvProtocol = "http"
	// TCPSrvProtocol is blah
	TCPSrvProtocol SrvProtocol = "tcp"
)

// ToConnector is blah
type ToConnector struct {
	To string `json:"to"`
}

// ToChannel is blah
type ToChannel struct {
	To map[string]string `json:"to"`
}

// Qos is blah
type Qos struct{}

// Name is blah
type Name string

// RefKind is blah
type RefKind string

const (
	// DeploymentKind is blah
	DeploymentKind RefKind = "deployment"
	// ServiceKind is blah
	ServiceKind RefKind = "service"
)

// Domain is blah
type Domain string

// Version is blah
type Version []int16

// Scope is blah
type Scope []ScopeElement

// ScopeElement is blah
type ScopeElement string

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// V3DeploymentList is a list of V3Deployment resources
type V3DeploymentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []V3Deployment `json:"items"`
}
