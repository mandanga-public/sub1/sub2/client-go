package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuHttpInbound is a top-level type
type KukuHttpInbound struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuHttpInboundSpec `json:"spec"`
}

// Cert is part of the KukuHttpInboundSpec
type Cert struct {
	Domain   string `json:"domain"`
	KukuCert string `json:"kukucert"`
}

// Node is part of the KukuHttpInboundSpec
type Node struct {
	IP string `json:"ip"`
}

// KukuHttpInboundSpec is the custom spec of KukuHttpInbound
type KukuHttpInboundSpec struct {
	TLS        bool    `json:"tls"`
	ClientCert bool    `json:"clientcert"`
	Websocket  *bool   `json:"websocket"`
	VHosts     []string `json:"vhosts"`
	Certs      []Cert  `json:"certs"`
	Nodes      []Node  `json:"nodes"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuHttpInboundList is a list of KukuHttpInbound resources
type KukuHttpInboundList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuHttpInbound `json:"items"`
}
