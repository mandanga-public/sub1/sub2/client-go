package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuDeployment is a top-level type
type KukuDeployment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuDeploymentSpec `json:"spec"`
	// Status						KukuComponentStatus `json:"status"`
}

// KukuDeploymentSpec is the custom spec of KukuDeployment
type KukuDeploymentSpec struct {
	Configuration   *KumoriConfiguration    `json:"configuration,omitempty"`
	Roles           []KumoriRoleArrangement `json:"roles"`
	SLAs            []KumoriRoleSLA         `json:"sla"`
	ServiceSelector metav1.LabelSelector    `json:"serviceSelector"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuDeploymentList is a list of KukuDeployment resources
type KukuDeploymentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuDeployment `json:"items"`
}
