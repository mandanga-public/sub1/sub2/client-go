package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuCert is a top-level type
type KukuCert struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Data              KukuCertData `json:"data"`
}

// KukuCertData is the data field of KukuCert
type KukuCertData struct {
	Domain string  `json:"domain"`
	Cert   []byte  `json:"cert"`
	Key    []byte  `json:"key"`
	Ca     *[]byte `json:"ca"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuCertList is a list of KukuCert resources
type KukuCertList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuCert `json:"items"`
}
