package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuVHost is a top-level type
type KukuVHost struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuVHostSpec `json:"spec"`
}

// KukuVHostSpec is the custom spec of KukuVHost
type KukuVHostSpec struct {
	VHost string `json:"vhost"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuVHostList is a list of KukuVHost resources
type KukuVHostList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuVHost `json:"items"`
}
