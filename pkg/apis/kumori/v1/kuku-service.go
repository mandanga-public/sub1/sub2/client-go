package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuService is a top-level type
type KukuService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	// Status						KukuComponentStatus `json:"status"`
	Spec KukuServiceSpec `json:"spec"`
}

// KukuServiceSpec is the custom spec of KukuService
type KukuServiceSpec struct {
	Channels      KumoriChannels                  `json:"channels"`
	Configuration *KumoriConfigurationDescription `json:"configuration,omitempty"`
	Connectors    []KumoriConnector               `json:"connectors"`
	Roles         []KumoriRole                    `json:"roles"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuServiceList is a list of KukuService resources
type KukuServiceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuService `json:"items"`
}
