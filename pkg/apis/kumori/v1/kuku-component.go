package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuComponent is a top-level type
type KukuComponent struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	// Status						KukuComponentStatus `json:"status"`
	Spec KukuComponentSpec `json:"spec"`
}

// KukuComponentSpec is the custom spec of KukuComponent
type KukuComponentSpec struct {
	Channels      KumoriChannels                  `json:"channels"`
	Configuration *KumoriConfigurationDescription `json:"configuration,omitempty"`
	Containers    []KumoriContainer               `json:"containers"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuComponentList is a list of KukuComponent resources
type KukuComponentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuComponent `json:"items"`
}
