/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package v1

import (
	"time"

	v1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis_ambassador/getambassador.io/v1"
	scheme "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated_ambassador/clientset/versioned/scheme"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// KubernetesEndpointResolversGetter has a method to return a KubernetesEndpointResolverInterface.
// A group's client should implement this interface.
type KubernetesEndpointResolversGetter interface {
	KubernetesEndpointResolvers(namespace string) KubernetesEndpointResolverInterface
}

// KubernetesEndpointResolverInterface has methods to work with KubernetesEndpointResolver resources.
type KubernetesEndpointResolverInterface interface {
	Create(*v1.KubernetesEndpointResolver) (*v1.KubernetesEndpointResolver, error)
	Update(*v1.KubernetesEndpointResolver) (*v1.KubernetesEndpointResolver, error)
	Delete(name string, options *metav1.DeleteOptions) error
	DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
	Get(name string, options metav1.GetOptions) (*v1.KubernetesEndpointResolver, error)
	List(opts metav1.ListOptions) (*v1.KubernetesEndpointResolverList, error)
	Watch(opts metav1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.KubernetesEndpointResolver, err error)
	KubernetesEndpointResolverExpansion
}

// kubernetesEndpointResolvers implements KubernetesEndpointResolverInterface
type kubernetesEndpointResolvers struct {
	client rest.Interface
	ns     string
}

// newKubernetesEndpointResolvers returns a KubernetesEndpointResolvers
func newKubernetesEndpointResolvers(c *GetambassadorV1Client, namespace string) *kubernetesEndpointResolvers {
	return &kubernetesEndpointResolvers{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the kubernetesEndpointResolver, and returns the corresponding kubernetesEndpointResolver object, and an error if there is any.
func (c *kubernetesEndpointResolvers) Get(name string, options metav1.GetOptions) (result *v1.KubernetesEndpointResolver, err error) {
	result = &v1.KubernetesEndpointResolver{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of KubernetesEndpointResolvers that match those selectors.
func (c *kubernetesEndpointResolvers) List(opts metav1.ListOptions) (result *v1.KubernetesEndpointResolverList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1.KubernetesEndpointResolverList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested kubernetesEndpointResolvers.
func (c *kubernetesEndpointResolvers) Watch(opts metav1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a kubernetesEndpointResolver and creates it.  Returns the server's representation of the kubernetesEndpointResolver, and an error, if there is any.
func (c *kubernetesEndpointResolvers) Create(kubernetesEndpointResolver *v1.KubernetesEndpointResolver) (result *v1.KubernetesEndpointResolver, err error) {
	result = &v1.KubernetesEndpointResolver{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		Body(kubernetesEndpointResolver).
		Do().
		Into(result)
	return
}

// Update takes the representation of a kubernetesEndpointResolver and updates it. Returns the server's representation of the kubernetesEndpointResolver, and an error, if there is any.
func (c *kubernetesEndpointResolvers) Update(kubernetesEndpointResolver *v1.KubernetesEndpointResolver) (result *v1.KubernetesEndpointResolver, err error) {
	result = &v1.KubernetesEndpointResolver{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		Name(kubernetesEndpointResolver.Name).
		Body(kubernetesEndpointResolver).
		Do().
		Into(result)
	return
}

// Delete takes name of the kubernetesEndpointResolver and deletes it. Returns an error if one occurs.
func (c *kubernetesEndpointResolvers) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *kubernetesEndpointResolvers) DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched kubernetesEndpointResolver.
func (c *kubernetesEndpointResolvers) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.KubernetesEndpointResolver, err error) {
	result = &v1.KubernetesEndpointResolver{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("kubernetesendpointresolvers").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
