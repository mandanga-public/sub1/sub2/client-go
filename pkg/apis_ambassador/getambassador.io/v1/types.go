package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// Our controllers works with Ambassador objects (CRDs), so we want generate
// code for them

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KubernetesEndpointResolver is a top-level type
type KubernetesEndpointResolver struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KubernetesEndpointResolverList is a list of KubernetesEndpointResolver resources
type KubernetesEndpointResolverList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KubernetesEndpointResolver `json:"items"`
}

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TLSContext is a top-level type
type TLSContext struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              TLSContextSpec `json:"spec"`
}

// TLSContextSpec is the custom spec of TLSContext
type TLSContextSpec struct {
	Hosts                 []string `json:"hosts"`
	Secret                string   `json:"secret"`
	CaSecret              string   `json:"ca_secret"`
	RedirectCleartextFrom int32    `json:"redirect_cleartext_from"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TLSContextList is a list of TLSContext resources
type TLSContextList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []TLSContext `json:"items"`
}

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Mapping is a top-level type
type Mapping struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              MappingSpec `json:"spec"`
}

// MappingSpec is the custom spec of Mapping
type MappingSpec struct {
	Host              string            `json:"host"`
	PrefixRegex       bool              `json:"prefix_regex"`
	Prefix            string            `json:"prefix"`
	Rewrite           string            `json:"rewrite"`
	Service           string            `json:"service"`
	Precedence        int               `json:"precedence"`
	Resolver          string            `json:"resolver"`
	LoadBalancer      map[string]string `json:"load_balancer"`
	Websocket         bool              `json:"use_websocket"`
	AddRequestHeaders map[string]string `json:"add_request_headers"`
	TimeoutMs         int               `json:"timeout_ms"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// MappingList is a list of Mapping resources
type MappingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []Mapping `json:"items"`
}

// ---------------------------------------------------------------------------
