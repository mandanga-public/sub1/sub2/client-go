/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	kumoriv1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeKukuCNs implements KukuCNInterface
type FakeKukuCNs struct {
	Fake *FakeKumoriV1
	ns   string
}

var kukucnsResource = schema.GroupVersionResource{Group: "kumori.systems", Version: "v1", Resource: "kukucns"}

var kukucnsKind = schema.GroupVersionKind{Group: "kumori.systems", Version: "v1", Kind: "KukuCN"}

// Get takes name of the kukuCN, and returns the corresponding kukuCN object, and an error if there is any.
func (c *FakeKukuCNs) Get(name string, options v1.GetOptions) (result *kumoriv1.KukuCN, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(kukucnsResource, c.ns, name), &kumoriv1.KukuCN{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCN), err
}

// List takes label and field selectors, and returns the list of KukuCNs that match those selectors.
func (c *FakeKukuCNs) List(opts v1.ListOptions) (result *kumoriv1.KukuCNList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(kukucnsResource, kukucnsKind, c.ns, opts), &kumoriv1.KukuCNList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &kumoriv1.KukuCNList{ListMeta: obj.(*kumoriv1.KukuCNList).ListMeta}
	for _, item := range obj.(*kumoriv1.KukuCNList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested kukuCNs.
func (c *FakeKukuCNs) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(kukucnsResource, c.ns, opts))

}

// Create takes the representation of a kukuCN and creates it.  Returns the server's representation of the kukuCN, and an error, if there is any.
func (c *FakeKukuCNs) Create(kukuCN *kumoriv1.KukuCN) (result *kumoriv1.KukuCN, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(kukucnsResource, c.ns, kukuCN), &kumoriv1.KukuCN{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCN), err
}

// Update takes the representation of a kukuCN and updates it. Returns the server's representation of the kukuCN, and an error, if there is any.
func (c *FakeKukuCNs) Update(kukuCN *kumoriv1.KukuCN) (result *kumoriv1.KukuCN, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(kukucnsResource, c.ns, kukuCN), &kumoriv1.KukuCN{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCN), err
}

// Delete takes name of the kukuCN and deletes it. Returns an error if one occurs.
func (c *FakeKukuCNs) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(kukucnsResource, c.ns, name), &kumoriv1.KukuCN{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeKukuCNs) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(kukucnsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &kumoriv1.KukuCNList{})
	return err
}

// Patch applies the patch and returns the patched kukuCN.
func (c *FakeKukuCNs) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *kumoriv1.KukuCN, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(kukucnsResource, c.ns, name, pt, data, subresources...), &kumoriv1.KukuCN{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCN), err
}
