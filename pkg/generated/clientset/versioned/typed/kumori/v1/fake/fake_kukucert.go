/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	kumoriv1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeKukuCerts implements KukuCertInterface
type FakeKukuCerts struct {
	Fake *FakeKumoriV1
	ns   string
}

var kukucertsResource = schema.GroupVersionResource{Group: "kumori.systems", Version: "v1", Resource: "kukucerts"}

var kukucertsKind = schema.GroupVersionKind{Group: "kumori.systems", Version: "v1", Kind: "KukuCert"}

// Get takes name of the kukuCert, and returns the corresponding kukuCert object, and an error if there is any.
func (c *FakeKukuCerts) Get(name string, options v1.GetOptions) (result *kumoriv1.KukuCert, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(kukucertsResource, c.ns, name), &kumoriv1.KukuCert{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCert), err
}

// List takes label and field selectors, and returns the list of KukuCerts that match those selectors.
func (c *FakeKukuCerts) List(opts v1.ListOptions) (result *kumoriv1.KukuCertList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(kukucertsResource, kukucertsKind, c.ns, opts), &kumoriv1.KukuCertList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &kumoriv1.KukuCertList{ListMeta: obj.(*kumoriv1.KukuCertList).ListMeta}
	for _, item := range obj.(*kumoriv1.KukuCertList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested kukuCerts.
func (c *FakeKukuCerts) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(kukucertsResource, c.ns, opts))

}

// Create takes the representation of a kukuCert and creates it.  Returns the server's representation of the kukuCert, and an error, if there is any.
func (c *FakeKukuCerts) Create(kukuCert *kumoriv1.KukuCert) (result *kumoriv1.KukuCert, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(kukucertsResource, c.ns, kukuCert), &kumoriv1.KukuCert{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCert), err
}

// Update takes the representation of a kukuCert and updates it. Returns the server's representation of the kukuCert, and an error, if there is any.
func (c *FakeKukuCerts) Update(kukuCert *kumoriv1.KukuCert) (result *kumoriv1.KukuCert, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(kukucertsResource, c.ns, kukuCert), &kumoriv1.KukuCert{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCert), err
}

// Delete takes name of the kukuCert and deletes it. Returns an error if one occurs.
func (c *FakeKukuCerts) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(kukucertsResource, c.ns, name), &kumoriv1.KukuCert{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeKukuCerts) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(kukucertsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &kumoriv1.KukuCertList{})
	return err
}

// Patch applies the patch and returns the patched kukuCert.
func (c *FakeKukuCerts) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *kumoriv1.KukuCert, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(kukucertsResource, c.ns, name, pt, data, subresources...), &kumoriv1.KukuCert{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuCert), err
}
