/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	kumoriv1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeKukuPersistentVolumes implements KukuPersistentVolumeInterface
type FakeKukuPersistentVolumes struct {
	Fake *FakeKumoriV1
	ns   string
}

var kukupersistentvolumesResource = schema.GroupVersionResource{Group: "kumori.systems", Version: "v1", Resource: "kukupersistentvolumes"}

var kukupersistentvolumesKind = schema.GroupVersionKind{Group: "kumori.systems", Version: "v1", Kind: "KukuPersistentVolume"}

// Get takes name of the kukuPersistentVolume, and returns the corresponding kukuPersistentVolume object, and an error if there is any.
func (c *FakeKukuPersistentVolumes) Get(name string, options v1.GetOptions) (result *kumoriv1.KukuPersistentVolume, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(kukupersistentvolumesResource, c.ns, name), &kumoriv1.KukuPersistentVolume{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuPersistentVolume), err
}

// List takes label and field selectors, and returns the list of KukuPersistentVolumes that match those selectors.
func (c *FakeKukuPersistentVolumes) List(opts v1.ListOptions) (result *kumoriv1.KukuPersistentVolumeList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(kukupersistentvolumesResource, kukupersistentvolumesKind, c.ns, opts), &kumoriv1.KukuPersistentVolumeList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &kumoriv1.KukuPersistentVolumeList{ListMeta: obj.(*kumoriv1.KukuPersistentVolumeList).ListMeta}
	for _, item := range obj.(*kumoriv1.KukuPersistentVolumeList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested kukuPersistentVolumes.
func (c *FakeKukuPersistentVolumes) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(kukupersistentvolumesResource, c.ns, opts))

}

// Create takes the representation of a kukuPersistentVolume and creates it.  Returns the server's representation of the kukuPersistentVolume, and an error, if there is any.
func (c *FakeKukuPersistentVolumes) Create(kukuPersistentVolume *kumoriv1.KukuPersistentVolume) (result *kumoriv1.KukuPersistentVolume, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(kukupersistentvolumesResource, c.ns, kukuPersistentVolume), &kumoriv1.KukuPersistentVolume{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuPersistentVolume), err
}

// Update takes the representation of a kukuPersistentVolume and updates it. Returns the server's representation of the kukuPersistentVolume, and an error, if there is any.
func (c *FakeKukuPersistentVolumes) Update(kukuPersistentVolume *kumoriv1.KukuPersistentVolume) (result *kumoriv1.KukuPersistentVolume, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(kukupersistentvolumesResource, c.ns, kukuPersistentVolume), &kumoriv1.KukuPersistentVolume{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuPersistentVolume), err
}

// Delete takes name of the kukuPersistentVolume and deletes it. Returns an error if one occurs.
func (c *FakeKukuPersistentVolumes) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(kukupersistentvolumesResource, c.ns, name), &kumoriv1.KukuPersistentVolume{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeKukuPersistentVolumes) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(kukupersistentvolumesResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &kumoriv1.KukuPersistentVolumeList{})
	return err
}

// Patch applies the patch and returns the patched kukuPersistentVolume.
func (c *FakeKukuPersistentVolumes) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *kumoriv1.KukuPersistentVolume, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(kukupersistentvolumesResource, c.ns, name, pt, data, subresources...), &kumoriv1.KukuPersistentVolume{})

	if obj == nil {
		return nil, err
	}
	return obj.(*kumoriv1.KukuPersistentVolume), err
}
