/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by informer-gen. DO NOT EDIT.

package v1

import (
	time "time"

	kumoriv1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	versioned "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated/clientset/versioned"
	internalinterfaces "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated/informers/externalversions/internalinterfaces"
	v1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/generated/listers/kumori/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	watch "k8s.io/apimachinery/pkg/watch"
	cache "k8s.io/client-go/tools/cache"
)

// KukuComponentInformer provides access to a shared informer and lister for
// KukuComponents.
type KukuComponentInformer interface {
	Informer() cache.SharedIndexInformer
	Lister() v1.KukuComponentLister
}

type kukuComponentInformer struct {
	factory          internalinterfaces.SharedInformerFactory
	tweakListOptions internalinterfaces.TweakListOptionsFunc
	namespace        string
}

// NewKukuComponentInformer constructs a new informer for KukuComponent type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewKukuComponentInformer(client versioned.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers) cache.SharedIndexInformer {
	return NewFilteredKukuComponentInformer(client, namespace, resyncPeriod, indexers, nil)
}

// NewFilteredKukuComponentInformer constructs a new informer for KukuComponent type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewFilteredKukuComponentInformer(client versioned.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers, tweakListOptions internalinterfaces.TweakListOptionsFunc) cache.SharedIndexInformer {
	return cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options metav1.ListOptions) (runtime.Object, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.KumoriV1().KukuComponents(namespace).List(options)
			},
			WatchFunc: func(options metav1.ListOptions) (watch.Interface, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.KumoriV1().KukuComponents(namespace).Watch(options)
			},
		},
		&kumoriv1.KukuComponent{},
		resyncPeriod,
		indexers,
	)
}

func (f *kukuComponentInformer) defaultInformer(client versioned.Interface, resyncPeriod time.Duration) cache.SharedIndexInformer {
	return NewFilteredKukuComponentInformer(client, f.namespace, resyncPeriod, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc}, f.tweakListOptions)
}

func (f *kukuComponentInformer) Informer() cache.SharedIndexInformer {
	return f.factory.InformerFor(&kumoriv1.KukuComponent{}, f.defaultInformer)
}

func (f *kukuComponentInformer) Lister() v1.KukuComponentLister {
	return v1.NewKukuComponentLister(f.Informer().GetIndexer())
}
