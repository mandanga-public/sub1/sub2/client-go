/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by informer-gen. DO NOT EDIT.

package externalversions

import (
	"fmt"

	v1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	cache "k8s.io/client-go/tools/cache"
)

// GenericInformer is type of SharedIndexInformer which will locate and delegate to other
// sharedInformers based on type
type GenericInformer interface {
	Informer() cache.SharedIndexInformer
	Lister() cache.GenericLister
}

type genericInformer struct {
	informer cache.SharedIndexInformer
	resource schema.GroupResource
}

// Informer returns the SharedIndexInformer.
func (f *genericInformer) Informer() cache.SharedIndexInformer {
	return f.informer
}

// Lister returns the GenericLister.
func (f *genericInformer) Lister() cache.GenericLister {
	return cache.NewGenericLister(f.Informer().GetIndexer(), f.resource)
}

// ForResource gives generic access to a shared informer of the matching type
// TODO extend this to unknown resources with a client pool
func (f *sharedInformerFactory) ForResource(resource schema.GroupVersionResource) (GenericInformer, error) {
	switch resource {
	// Group=kumori.systems, Version=v1
	case v1.SchemeGroupVersion.WithResource("kukucns"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuCNs().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukucerts"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuCerts().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukucomponents"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuComponents().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukudeployments"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuDeployments().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukuhttpinbounds"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuHttpInbounds().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukulinks"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuLinks().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukupersistentvolumes"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuPersistentVolumes().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukuservices"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuServices().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("kukuvhosts"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().KukuVHosts().Informer()}, nil
	case v1.SchemeGroupVersion.WithResource("v3deployments"):
		return &genericInformer{resource: resource.GroupResource(), informer: f.Kumori().V1().V3Deployments().Informer()}, nil

	}

	return nil, fmt.Errorf("no informer found for %v", resource)
}
