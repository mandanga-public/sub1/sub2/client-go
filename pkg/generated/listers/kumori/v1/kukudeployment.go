/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuDeploymentLister helps list KukuDeployments.
type KukuDeploymentLister interface {
	// List lists all KukuDeployments in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuDeployment, err error)
	// KukuDeployments returns an object that can list and get KukuDeployments.
	KukuDeployments(namespace string) KukuDeploymentNamespaceLister
	KukuDeploymentListerExpansion
}

// kukuDeploymentLister implements the KukuDeploymentLister interface.
type kukuDeploymentLister struct {
	indexer cache.Indexer
}

// NewKukuDeploymentLister returns a new KukuDeploymentLister.
func NewKukuDeploymentLister(indexer cache.Indexer) KukuDeploymentLister {
	return &kukuDeploymentLister{indexer: indexer}
}

// List lists all KukuDeployments in the indexer.
func (s *kukuDeploymentLister) List(selector labels.Selector) (ret []*v1.KukuDeployment, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuDeployment))
	})
	return ret, err
}

// KukuDeployments returns an object that can list and get KukuDeployments.
func (s *kukuDeploymentLister) KukuDeployments(namespace string) KukuDeploymentNamespaceLister {
	return kukuDeploymentNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuDeploymentNamespaceLister helps list and get KukuDeployments.
type KukuDeploymentNamespaceLister interface {
	// List lists all KukuDeployments in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuDeployment, err error)
	// Get retrieves the KukuDeployment from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuDeployment, error)
	KukuDeploymentNamespaceListerExpansion
}

// kukuDeploymentNamespaceLister implements the KukuDeploymentNamespaceLister
// interface.
type kukuDeploymentNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuDeployments in the indexer for a given namespace.
func (s kukuDeploymentNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuDeployment, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuDeployment))
	})
	return ret, err
}

// Get retrieves the KukuDeployment from the indexer for a given namespace and name.
func (s kukuDeploymentNamespaceLister) Get(name string) (*v1.KukuDeployment, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukudeployment"), name)
	}
	return obj.(*v1.KukuDeployment), nil
}
