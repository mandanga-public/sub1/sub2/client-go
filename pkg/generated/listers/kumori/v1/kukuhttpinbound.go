/*
Copyright Kumori.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/mandanga-public/sub1/sub2/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuHttpInboundLister helps list KukuHttpInbounds.
type KukuHttpInboundLister interface {
	// List lists all KukuHttpInbounds in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error)
	// KukuHttpInbounds returns an object that can list and get KukuHttpInbounds.
	KukuHttpInbounds(namespace string) KukuHttpInboundNamespaceLister
	KukuHttpInboundListerExpansion
}

// kukuHttpInboundLister implements the KukuHttpInboundLister interface.
type kukuHttpInboundLister struct {
	indexer cache.Indexer
}

// NewKukuHttpInboundLister returns a new KukuHttpInboundLister.
func NewKukuHttpInboundLister(indexer cache.Indexer) KukuHttpInboundLister {
	return &kukuHttpInboundLister{indexer: indexer}
}

// List lists all KukuHttpInbounds in the indexer.
func (s *kukuHttpInboundLister) List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuHttpInbound))
	})
	return ret, err
}

// KukuHttpInbounds returns an object that can list and get KukuHttpInbounds.
func (s *kukuHttpInboundLister) KukuHttpInbounds(namespace string) KukuHttpInboundNamespaceLister {
	return kukuHttpInboundNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuHttpInboundNamespaceLister helps list and get KukuHttpInbounds.
type KukuHttpInboundNamespaceLister interface {
	// List lists all KukuHttpInbounds in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error)
	// Get retrieves the KukuHttpInbound from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuHttpInbound, error)
	KukuHttpInboundNamespaceListerExpansion
}

// kukuHttpInboundNamespaceLister implements the KukuHttpInboundNamespaceLister
// interface.
type kukuHttpInboundNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuHttpInbounds in the indexer for a given namespace.
func (s kukuHttpInboundNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuHttpInbound))
	})
	return ret, err
}

// Get retrieves the KukuHttpInbound from the indexer for a given namespace and name.
func (s kukuHttpInboundNamespaceLister) Get(name string) (*v1.KukuHttpInbound, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukuhttpinbound"), name)
	}
	return obj.(*v1.KukuHttpInbound), nil
}
